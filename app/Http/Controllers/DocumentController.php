<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\Request;

use Storage;

class DocumentController extends Controller
{
    public function createDocument(Request $request){
        try {
            $id = $request->id;
            
            $year = date('Y');
            $month = date('m');

            $docu_id_count = Document::whereYear('created_at', $year)
                                    ->whereMonth('created_at', $month)
                                    ->get();

            $last_id = $docu_id_count->count();

            $document = $id == "" ? new Document : Document::find($id);

            $file =  $request->file;
            $filename =  $request->fileuploaded;

            if(is_uploaded_file($file)){
                if(Storage::exists('upload/' . $filename)){
                    Storage::delete('upload/' . $filename);
                }
                $upload_path = public_path('upload');
                $file_name = $request->file->getClientOriginalName();
                $generated_new_name = time() . '.' . $request->file->getClientOriginalExtension();
                $request->file->move($upload_path, $generated_new_name);

                $document->fileAttchmnt = $generated_new_name;
            }
            if($id == ""){
                $document->document_no = $year . '-' . $month . '-' . str_pad(($last_id + 1), 4, '0', STR_PAD_LEFT);
            }
            
            $document->datetimeReceived = $request->dateTimeReceived;
            $document->docDeadline = $request->docDeadline;
            $document->docClassification = $request->classification;
            $document->subject = $request->subject;
            $document->sender = $request->sender;
            $document->office_origin = $request->officeOrigin;
            $document->emp_id = $request->userid;

            $document->save();

            return response()->json([
                'status' => 'success',
                'message' => $id == "" ? 'Successfully added' : 'Successfully added',
                'document_no' => $document->document_no
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error', 
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function getMyDocuments($office, $emp_id){
        try {
            $doc = Document::where('office_origin', '=', $office)
                            ->where('emp_id', '=', $emp_id)
                            ->whereRaw('( status is null OR status <> "deleted" )')
                            ->orderByDesc('created_at')
                            ->get();

            return $doc;


        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error', 
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function deleteDocument(Request $request){
        try {
            $id = $request->id;

            $document = Document::find($id);

            $document->status = 'deleted';

            $document->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Successfully deleted',
            ], 200);


        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error', 
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
