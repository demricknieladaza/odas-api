<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->string('document_no');
            $table->dateTime('datetimeReceived');
            $table->date('docDeadline')->nullable();
            $table->date('artaDeadline')->nullable();
            $table->string('docClassification');
            $table->text('subject');
            $table->text('sender');
            $table->string('fileAttchmnt');
            $table->string('office_origin');
            $table->string('emp_id');
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
